package ir.maktab43.domains.enums;

public enum NewsType {

    ARTICLE, GALLERY, VOICE, VIDEO
}

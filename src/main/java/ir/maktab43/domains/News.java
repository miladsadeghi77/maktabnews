package ir.maktab43.domains;

import ir.maktab43.domains.enums.NewsType;
import ir.maktab43.domains.util.Gallery;
import ir.maktab43.domains.util.Photo;
import ir.maktab43.domains.util.Video;
import ir.maktab43.domains.util.Voice;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class News {

    private Long id;

    private String title;

    private String brief;

    private String content;

    private Date createDate;

    private NewsType type;

    //    @ManyToOne
    private User creator;

    private Photo banner;

    private Video video;

    private Voice voice;

    private Gallery gallery;

    //    @ManyToOne
    private Category category;

    //  @ManyToMany
    private Set<Tag> tags = new HashSet<>();

    public NewsType getType() {
        return type;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public Voice getVoice() {
        return voice;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    public Gallery getGallery() {
        return gallery;
    }

    public void setGallery(Gallery gallery) {
        this.gallery = gallery;
    }

    public void setType(NewsType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Photo getBanner() {
        return banner;
    }

    public void setBanner(Photo banner) {
        this.banner = banner;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
}

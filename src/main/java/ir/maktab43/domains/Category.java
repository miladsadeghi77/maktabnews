package ir.maktab43.domains;

import java.util.HashSet;
import java.util.Set;

public class Category {

    private Long id;

    private String title;

    private String description;

    private Boolean isChosenForHeader;

    //    @ManyToMany
//    @OneToOne
//    @OneToMany
//    @ManyToOne *
    private Category parent;

    //    @OneToMany
    private Set<Category> childrenCategories = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getChosenForHeader() {
        return isChosenForHeader;
    }

    public void setChosenForHeader(Boolean chosenForHeader) {
        isChosenForHeader = chosenForHeader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public Set<Category> getChildrenCategories() {
        return childrenCategories;
    }

    public void setChildrenCategories(Set<Category> childrenCategories) {
        this.childrenCategories = childrenCategories;
    }
}
